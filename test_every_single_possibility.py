# I learned that the code that run in the plane is actually bug free
# because it tests again every single possible input of the function
# this is an attempt to do just that

from cron_challenge import Task
def number_today(line):
    count = 0
    for hour in range(0, 24):
        for minute in range(0, 60):
            hour_minute = "{}:{}".format(hour, minute)
            output = str(Task(line, hour_minute))
            if "today" in output:
                count += 1
            # print(hour_minute, output)
    return count

if __name__=="__main__":
    for hour in range(0, 24):
        for minute in range(0, 60):
            line0 = "{} {} /bin/run_me_daily".format(minute, hour)
            assert number_today(line0) == hour*60+minute+1
    print("----------------------")

    for minute in range(0, 60):
        line1 = "{} * /bin/run_me_hourly".format(minute)
        assert number_today(line1) == 23*60 + (minute + 1)
    print("----------------------")

    line2 = "* * /bin/run_me_every_minute"
    assert number_today(line2) == 24*60
    print("----------------------")

    for hour in range(0, 24):
        line3 = "* {} /bin/run_me_sixty_times".format(hour)
        assert number_today(line3) == (hour+1)*60
    print("----------------------")

    print("All Passed")


