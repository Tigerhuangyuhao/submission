#!/usr/bin/python
import datetime

class Task:
    def __init__(self, line, current_time):
        # dont use strptime just in case input like 1:20
        assert ":" in current_time, "Weird Format of input time"
        self.time = datetime.datetime(1995, 8, 30)
        self.current_time = datetime.time(
            *[int(i) for i in current_time.split(":")]
        )
        self.time, self.command = self.parse(line)

    def parse(self, line):

        # TODO: test for this function
        # test case 16:46 with config 45 * should be 17 45 today
        # test case 23:46 with config 45 * should be 00 46 tomorrow

        # DRY: Don't Repeat Youself, hide it from other methods of this class
        def parse_time(minute_str, hour_str):
            if minute_str == "*" and hour_str == "*":
                minute = self.current_time.minute
                hour = self.current_time.hour
            elif minute_str == "*": # * 19 /bin/run_me_sixty_times
                hour = int(hour_str)
                if self.current_time.hour == hour:
                    minute = self.current_time.minute
                else:
                    minute = 0
            elif hour_str == "*": # test cases 16:09 16:11 16:10 - * 10
                minute = int(minute_str)
                if self.current_time.minute > minute: # next hour
                    hour = self.current_time.hour + 1
                    if hour == 24: hour = 0
                else:
                    hour = self.current_time.hour
            else:
                minute = int(minute_str)
                hour = int(hour_str)

            return minute, hour


        assert line.count(" ") == 2, "line should have exactly 2 spaces"
        minute_str, hour_str, command = line.rsplit(' ')
        minute, hour = parse_time(minute_str, hour_str) # for readability

        t = datetime.time(hour, minute)

        return t, command

    def __str__(self):
        return "{}:{:02} {} - {}".format(
                self.time.hour,
                self.time.minute,
                self.today_or_tommorow(),
                self.command
        )

    def today_or_tommorow(self):
        if self.time < self.current_time:
            return "tomorrow"
        else:
            return "today"

if __name__ == "__main__":
    import sys
    assert len(sys.argv) == 2, "Run it like cron_challenge.py 16:10 < test/config"
    for line in sys.stdin:
        print(Task(line.strip(), sys.argv[1]))

